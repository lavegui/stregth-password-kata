# Password Strength Kata

The objetive is build a simple validator for a password

## Rules

### Iteration 1

* The length of the password must be at least 7 characters
* The password must contain at least 1 digit
* The password must contain at least 1 capital letter
* The password must contain at least 1 lower case letter
* The password must contain at least 1 underscore

### Iteration 2

We need to provide feedback to the user about the strength of their password
* Provide the user with a list of reasons why their password is 'weak'

### Iteration 3

* Make async

## Install

```
yarn install
```

## Run tests

```
yarn test
```

## Avoid intellij warnings

Go to *Settings/Languages & frameworks/JavaScript/Libraries* and download:
* @types/node
* @types/jest
