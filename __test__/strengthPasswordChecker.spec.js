import passwordChecker from "../src/strengthPasswordChecker";

describe('strength password checker', () => {

    it('it is the expected checker', () => {
        expect(passwordChecker().whoAmI).toBe('I am the strength password checker');
    });
});
